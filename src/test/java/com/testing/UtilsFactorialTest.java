package com.testing;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.util.Random;

import org.junit.Ignore;
import org.junit.Test;

public class UtilsFactorialTest {

  @Ignore
  @Test
  public void simpleFactorial() {
    BigInteger result = Utils.computeFactorial(3);
    assertEquals(BigInteger.valueOf(6), result);
  }

  @Test(timeout = 10)
  public void testFactorialWithTimeout() {
    @SuppressWarnings("unused")
    BigInteger result = Utils.computeFactorial(new Random().nextInt());
  }

  @Test
  public void tryFactorialWithTimeout() {
    Integer factorial = 5;
    System.out.println("Ищем " + factorial + "!");
    BigInteger result = Utils.testFactorialWithTimeout(factorial);
    System.out.println("Результат:" + result);
  }


}
