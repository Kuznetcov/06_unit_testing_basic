package com.testing;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilsConcatenateTest {

  @Test
  public void concatenateSimpleWords() {
    String result = Utils.concatenateWords("Foot", "ball");
    assertEquals("Football", result);
  }

  @Test
  public void concatenateNulls() {
    String result = Utils.concatenateWords(null, null);
    assertEquals("nullnull", result);
  }

  @Test
  public void concatenateNewStrings() {
    String result = Utils.concatenateWords(new String(), new String());
    assertEquals("", result);
  }

  @Test
  public void concatenateEmptyWords() {
    String result = Utils.concatenateWords("", "");
    assertEquals("", result);
  }
  
  @Test
  public void concatenateNonLatin() {
    String result = Utils.concatenateWords("Нога", "мяч");
    assertEquals("Ногамяч", result);
  }

  @Test
  public void concatenateOneWord() {
    String result = Utils.concatenateWords("Нога мяч");
    assertEquals("Ногамяч", result);
  }

  @Test
  public void concatenateOneLongWord() {
    String result = Utils.concatenateWords("Белеет парус одинокий "
        + "В тумане моря голубом!.. " + "Что ищет он в стране далекой? "
        + "Что кинул он в краю родном?.. " + "Играют волны — ветер свищет, "
        + "И мачта гнется и скрипит… " + "Увы! он счастия не ищет, " + "И не от счастия бежит! "
        + "Под ним струя светлей лазури, " + "Над ним луч солнца золотой… "
        + "А он, мятежный, просит бури, " + "Как будто в бурях есть покой!");
    System.out.println(result);
  }
}
