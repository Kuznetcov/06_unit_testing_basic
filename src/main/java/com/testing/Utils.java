package com.testing;

import java.math.BigInteger;

public class Utils {

  public static BigInteger computeFactorial(Integer n) {
    BigInteger result = BigInteger.ONE;
    for (int i = 1; i <= n; ++i)
      result = result.multiply(BigInteger.valueOf(i));
    return result;
  }

  public static String concatenateWords(String word1, String word2) {
    String result;
    StringBuilder buffer = new StringBuilder();
    buffer.append(word1).append(word2);
    Utils.deleteSpaces(buffer);
    result = buffer.toString();
    return result;
  }

  public static String concatenateWords(String word1) {
    String result;
    StringBuilder buffer = new StringBuilder();
    buffer.append(word1);
    Utils.deleteSpaces(buffer);
    result = buffer.toString();
    return result;
  }

  private static StringBuilder deleteSpaces(StringBuilder source) {
    while (source.lastIndexOf(" ") != -1) {
      source.deleteCharAt(source.lastIndexOf(" "));
    }
    return source;
  }


  public static BigInteger testFactorialWithTimeout(Integer n) {

    final long startTime = System.nanoTime();
    final long breakPoint = startTime + 300000;
    long pause = startTime + 10000;
    System.out.println(startTime + " " + breakPoint + " " + pause);
    BigInteger result = BigInteger.ONE;
    long i = 1;
    while (System.nanoTime() < breakPoint && i <= n) {
      result = result.multiply(BigInteger.valueOf(i));
      i++;

      if (System.nanoTime() > pause) {
        try {
          pause += 10000;
          Thread.sleep(0);
          System.out.println(result);
        } catch (InterruptedException e) {
          System.out.println(
              "ПОЙМАЛ InterruptedException! Как поймал? Откуда второй поток? Нет у нас вторых потоков!");
        }
      }
    }
    long time = System.nanoTime();
    System.out.println(time);
    return result;
  }
}
